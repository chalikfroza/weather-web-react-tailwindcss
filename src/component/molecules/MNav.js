import React from "react"

const MNav = ({ isTemp, funcSetTemp }) => {
  return (
    <div className="container mx-auto py-5">
      <div className="flex items-center">
        <div className="w-full flex justify-between items-center">
          <p className="text-white text-xl font-bold">Weather app</p>
          <div className="flex justify-between items-center">
            <div
              className="bg-cyan-900 rounded-full w-8 h-8 flex justify-center items-center mx-1 cursor-pointer hover:bg-cyan-700"
              onClick={() => funcSetTemp(isTemp)}
            >
              {isTemp ? (
                // <p >℃</p>
                <span className="mx-auto text-white text-lg font-bold">
                  &#8451;
                </span>
              ) : (
                <span className="mx-auto text-white text-lg font-bold">
                  &#8457;
                </span>
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default MNav
