/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable jsx-a11y/alt-text */
import React, { useState, useEffect } from "react"
import { Raining, Cloudy, Snowy, Clear, Sunny, Mist } from "../../assets/images"
import axios from "axios"
import { Icon } from "@iconify/react"

const MCard = ({ id, data, funcUnSetData, isTemp }) => {
  const [weather, setWeather] = useState({})
  const [isLoading, setIsLoading] = useState(false)

  const weatherCondition = {
    rain: Raining,
    cloudy: Cloudy,
    snow: Snowy,
    clear: Clear,
    sunny: Sunny,
    overcast: Cloudy,
    shower: Cloudy,
    thunderstorm: Raining,
    sleet: Snowy,
    ice: Snowy,
    drizzle: Snowy,
    fog: Mist,
    blizzard: Snowy,
    mist: Mist,
  }

  const convertToF = (celsius) => {
    let fahrenheit = (celsius * 9) / 5 + 32
    return fahrenheit
  }

  const selectImage = (val) => {
    const lowVal = val.toLowerCase()
    const words = lowVal.split(/\W/)
    let updateData = []
    updateData = words.filter((item) => {
      const rain = item.toLowerCase().includes("rain".toLowerCase())
      const cloudy = item.toLowerCase().includes("cloudy".toLowerCase())
      const snow = item.toLowerCase().includes("snow".toLowerCase())
      const clear = item.toLowerCase().includes("clear".toLowerCase())
      const sunny = item.toLowerCase().includes("sunny".toLowerCase())
      const overcast = item.toLowerCase().includes("overcast".toLowerCase())
      const shower = item.toLowerCase().includes("shower".toLowerCase())
      const sleet = item.toLowerCase().includes("sleet".toLowerCase())
      const ice = item.toLowerCase().includes("ice".toLowerCase())
      const drizzle = item.toLowerCase().includes("drizzle".toLowerCase())
      const fog = item.toLowerCase().includes("fog".toLowerCase())
      const blizzard = item.toLowerCase().includes("blizzard".toLowerCase())
      const mist = item.toLowerCase().includes("mist".toLowerCase())
      const thunderstorm = item
        .toLowerCase()
        .includes("thunderstorm".toLowerCase())
      if (rain) {
        return rain
      } else if (cloudy) {
        return cloudy
      } else if (snow) {
        return snow
      } else if (clear) {
        return clear
      } else if (sunny) {
        return sunny
      } else if (overcast) {
        return overcast
      } else if (shower) {
        return shower
      } else if (thunderstorm) {
        return thunderstorm
      } else if (sleet) {
        return sleet
      } else if (ice) {
        return ice
      } else if (drizzle) {
        return drizzle
      } else if (fog) {
        return fog
      } else if (blizzard) {
        return blizzard
      } else if (mist) {
        return mist
      } else {
        return null
      }
    })
    return weatherCondition[updateData]
  }
  useEffect(async () => {
    const params = {
      access_key: "8ab0fb7e7020409fbae152528211712",
      query: data.city,
    }
    await setIsLoading(true)
    const result = await axios
      .get(
        `https://api.weatherapi.com/v1/current.json?key=${params.access_key}&q=${params.query}}&aqi=no`
      )
      .then(function (response) {
        setWeather(response.data)
      })
      .catch(function (error) {
        console.log(error)
      })
    await setIsLoading(false)
  }, [])
  console.log(weather)

  return (
    <div
      id={id}
      className="bg-white bg-cover bg-center group rounded-lg overflow-hidden shadow-lg hover:shadow-2xl transition duration-300 ease-in-out mx-3"
      style={{
        height: "70vh",
        width: "30vw",
      }}
    >
      {isLoading ? null : Object.keys(weather).length !== 0 ? (
        <>
          <div
            className="group-hover:opacity-90 transition duration-300 ease-in-out px-10 py-5 object-none bg-cover bg-center"
            style={{
              backgroundImage: `url(${selectImage(
                weather.current.condition.text
              )}`,
              height: "55vh",
              width: "30vw",
            }}
          >
            <div className="flex justify-between">
              <div
                className="bg-cyan-900 rounded-full w-8 h-8 flex justify-center items-center mx-1 cursor-pointer hover:bg-cyan-700"
                onClick={() => funcUnSetData(data.id)}
              >
                <Icon
                  icon="eva:close-outline"
                  className="text-white transition duration-500"
                />
              </div>
              {isTemp ? (
                // <p >℃</p>
                <span className="text-white text-lg font-bold">&#8451;</span>
              ) : (
                <span className="text-white text-lg font-bold">&#8457;</span>
              )}
            </div>
            <div
              className="flex flex-col justify-center items-center"
              style={{
                height: "50vh",
              }}
            >
              <p className="text-white text-4xl font-bold my-1 text-center">
                {weather.current.condition.text}
              </p>
              <p className="text-white text-xl text-center my-1">{`${weather.location.name}, ${weather.location.country}`}</p>

              {isTemp ? (
                <p className="text-white text-5xl font-bold my-1">
                  {`${weather.current.temp_c}`}°
                </p>
              ) : (
                <p className="text-white text-5xl font-bold my-1">
                  {`${weather.current.temp_f}`}°
                </p>
              )}
            </div>
          </div>
          <div
            className="px-10 flex justify-between items-center"
            style={{
              height: "15vh",
              width: "30vw",
            }}
          >
            <div className="flex flex-col justify-between items-center">
              <p className="text-lg font-light text-cyan-700">Wind</p>
              <p className="text-xl font-semibold text-cyan-700">{`${weather.current.wind_kph}km/h`}</p>
            </div>
            <div className="flex flex-col justify-between items-center">
              <p className="text-lg font-light text-cyan-700">Humadity</p>
              <p className="text-xl font-semibold text-cyan-700">{`${weather.current.humidity}%`}</p>
            </div>
            <div className="flex flex-col justify-between items-center">
              <p className="text-lg font-light text-cyan-700">UV Index</p>
              <p className="text-xl font-semibold text-cyan-700">{`${weather.current.uv}`}</p>
            </div>
          </div>
        </>
      ) : null}
    </div>
  )
}

export default MCard
