import Clear from './Clear.png'
import Cloudy from './Cloudy.png'
import Raining from './Raining.png'
import Snowy from './Snowy.png'
import Sunny from './Sunny.png'
import Mist from './Mist.png'

export {
  Clear,
  Cloudy,
  Raining,
  Snowy,
  Sunny,
  Mist
}