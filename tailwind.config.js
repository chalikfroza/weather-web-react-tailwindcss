
module.exports = {
  content: [
    "./src/**/*.{js,jsx}",
  ],
  theme: {
    theme: {
      screens: {
        sm: '480px',
        md: '768px',
        lg: '976px',
        xl: '1440px',
      },
      colors: {
        'primary': '#22577E',
        'secondary': '#5584AC',
        'tersier': '#95D1CC',
        'gray': '#F6F2D4',
        'white': '#FFFFFF'
      },
      fontFamily: {
        Poppins: 'Poppins',
        Montserrat: 'Montserrat'
      },
      extend: {
        spacing: {
          '128': '32rem',
          '144': '36rem',
        },
        borderRadius: {
          '4xl': '2rem',
        }
      }
    }
  },
  plugins: [],
}
